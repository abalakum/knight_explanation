window.onload = function() {
    const canvasElements = ['canvas1', 'canvas2', 'canvas3'];
    const canvases = [];
    const contexts = [];
    let dpi;
    if (window.devicePixelRatio !== undefined) {
        dpi = window.devicePixelRatio;
    } else {
        dpi = 1;
    }

    function colorSquare(context, color, x, y, squareSize) {
        context.fillStyle = color;
        context.fillRect(x * squareSize, y * squareSize, squareSize, squareSize);
        context.strokeStyle = 'black';
        context.strokeRect(x * squareSize, y * squareSize, squareSize, squareSize);
    }

    function drawLTrace (context, prevX, prevY, currX, currY, squareSize) {
        const prevXpos = prevX * squareSize + squareSize / 2;
        const prevYpos = prevY * squareSize + squareSize / 2;
        const currXpos = currX * squareSize + squareSize / 2;
        const currYpos = currY * squareSize + squareSize / 2;

        // Draw two lines forming an L-shape
        context.beginPath();
        context.moveTo(prevXpos, prevYpos);
        context.lineTo(currXpos, prevYpos);
        context.lineTo(currXpos, currYpos);
        context.stroke();

        // Draw two dots
        context.beginPath();
        context.arc(prevXpos, prevYpos, squareSize / 8, 0, 2 * Math.PI);
        context.arc(currXpos, currYpos, squareSize / 8, 0, 2 * Math.PI);
        context.fillStyle = 'black';
        context.fill();
    }

    function setupFirstCanvas (canvas, context, dpi) {
        const redSquares = [
            [2, 2]
        ];
        const blueSquares = [
            [1, 0], [3, 0],
            [0, 1], [4, 1],
            [0, 3], [4, 3],
            [1, 4], [3, 4]
        ];
    
        let style_size = Number(getComputedStyle(canvas).getPropertyValue("width").slice(0, -2));
        let squareSize = style_size / 5; 
    
        // Scale all drawing operations by the dpi to mirror the canvas scaling
        context.scale(dpi, dpi);
    
        // Draw 5x5 grid
        context.fillStyle = '#fff';
        context.strokeStyle = 'black';
        for(let x = 0; x < 5; x++) {
            for(let y = 0; y < 5; y++) {
                if (blueSquares.some(pos => pos[0] === x && pos[1] === y)) {
                    colorSquare(context, 'blue', x, y, squareSize);
                } else if (redSquares.some(pos => pos[0] === x && pos[1] === y)) {
                    colorSquare(context, 'red', x, y, squareSize);
                } else {
                    colorSquare(context, '#fff', x, y, squareSize);
                }
            }
        }
    
        // Draw a thicker black border around the 3x3 inner grid
        context.lineWidth = 3;
        context.strokeStyle = 'black';
        context.strokeRect(squareSize, squareSize, 3 * squareSize, 3 * squareSize);
        context.lineWidth = 1; // reset line width
    }

    function setupSecondCanvas(canvas, context, dpi) {
        const knightPath4x4 = [
            [0, 0], [1, 2], [2, 0], [3, 2], [1, 3],
            [0, 1], [2, 2], [3, 0], [1, 1], [2, 3],
            [3, 1], [1, 0], [0, 2], [2, 1], [3, 3],
            [0, 3] 
        ];

        let style_size = Number(getComputedStyle(canvas).getPropertyValue("width").slice(0, -2));
        let squareSize = style_size / 4;
        let currentStep = 1;
    
        // Scale all drawing operations by the dpi to mirror the canvas scaling
        context.scale(dpi, dpi);
    
        function drawGrid() {
            // Clear canvas
            context.clearRect(0, 0, canvas.width, canvas.height);
    
            // Draw 4x4 grid
            for(let x = 0; x < 4; x++) {
                for(let y = 0; y < 4; y++) {
                    if (knightPath4x4[currentStep - 1][0] === x && knightPath4x4[currentStep - 1][1] === y) {
                        colorSquare(context, 'blue', x, y, squareSize);
                    } else if (knightPath4x4.slice(0, currentStep).some(pos => pos[0] === x && pos[1] === y)) {
                        colorSquare(context, 'red', x, y, squareSize);
                    } else {
                        colorSquare(context, '#fff', x, y, squareSize);
                    }
                }
            }

            // If there's a previous step, draw marker to indicate path
            if (currentStep > 1) {
                let prevPos = knightPath4x4[currentStep - 2];
                const currPos = knightPath4x4[currentStep - 1];

                // Hard code the last step b/c it doesn't follow from the previous one
                if (currentStep === knightPath4x4.length) {
                    prevPos = [1,1]
                }
        
                drawLTrace(context, prevPos[0], prevPos[1], currPos[0], currPos[1], squareSize);
            }
        }

        function advanceSequence() {
            if (currentStep < knightPath4x4.length) {
                currentStep++;
                drawGrid();
            }
        }
        
        function retreatSequence() {
            if (currentStep > 1) {
                currentStep--;
                drawGrid();
            }
        }
    
        // Initial draw
        drawGrid();

        // Set up buttons
        const leftButton = document.getElementById("left-button");
        const rightButton = document.getElementById("right-button");
        leftButton.addEventListener("click", retreatSequence);
        rightButton.addEventListener("click", advanceSequence);

        // Set up keys
        document.addEventListener('keydown', function(event) {
            const key = event.key;
            const rect = canvas.getBoundingClientRect();
            const isInView = (rect.top >= 0) && (rect.bottom <= window.innerHeight);

            if (isInView) {
                if (key === 'ArrowLeft') {
                    retreatSequence();
                } else if (key === 'ArrowRight') {
                    advanceSequence();
                }
            }
        });
    }

    function setupThirdCanvas(canvas, context, dpi) {
        let style_size = Number(getComputedStyle(canvas).getPropertyValue("width").slice(0, -2));
        let size = 5; // Initial size set to 5
        let selected_position = null;
    
        // Scale all drawing operations by the dpi to mirror the canvas scaling
        context.scale(dpi, dpi);
    
        function drawGrid() {
            let squareSize = style_size / size;

            // Clear canvas
            context.clearRect(0, 0, canvas.width, canvas.height);
    
            // Draw the grid
            for(let x = 0; x < size; x++) {
                for(let y = 0; y < size; y++) {
                    colorSquare(context, '#fff', x, y, squareSize);
                }
            }

            // Draw a thicker black border around the (n-1) by (n-1) inner grid
            if (size > 1) {
                context.lineWidth = 3;
                context.strokeStyle = 'black';
                context.strokeRect(0, 0, (size - 1) * squareSize, (size - 1) * squareSize);
                context.lineWidth = 1; // reset line width for future drawing
            }

            // Draw the markings corresponding to the selected squares
            if (selected_position != null) {
                x1 = selected_position[0];
                y1 = selected_position[1];
                let x2;
                let y2;
                if (x1 === (size - 1)) {
                    if (y1 + 1 < (size - 1)) {
                        x2 = x1 - 2;
                        y2 = y1 + 1;
                    } else {
                        x2 = x1 - 2;
                        y2 = y1 - 1;
                    }
                } else {
                    if (x1 + 1 < (size - 1)) {
                        y2 = y1 - 2;
                        x2 = x1 + 1;
                    } else {
                        y2 = y1 - 2;
                        x2 = x1 - 1;
                    }
                }

                colorSquare(context, 'red', x1, y1, squareSize);
                colorSquare(context, 'red', x2, y2, squareSize);
                drawLTrace(context, x2, y2, x1, y1, squareSize);
            }
        }

        // Draw the grid initially
        drawGrid();

        // Set up buttons
        const sizeButtons = document.getElementsByClassName('size-button');
        for (let button of sizeButtons) {
            button.addEventListener('click', function() {
                const n = parseInt(this.innerText);
                
                // Update active button
                const sizeButtons = document.getElementsByClassName('size-button');
                for (let button of sizeButtons) {
                    button.classList.remove('active');
                }
                document.getElementById(`size${n}`).classList.add('active');

                // Redraw grid
                size = n;
                selected_position = null;
                drawGrid();
            });
        }

        // Set up mouse handlers for selecting a specific square
        canvas.addEventListener('mousemove', function(e) {
            let squareSize = style_size / size;

            // Calculate the x and y position of the mouse in the grid
            const rect = canvas.getBoundingClientRect();
            const x = Math.floor((e.clientX - rect.left) / squareSize);
            const y = Math.floor((e.clientY - rect.top) / squareSize);

            // Check if the mouse is outside the (n-1) x (n-1) subgrid
            if (x >= size - 1 || y >= size - 1) {
                selected_position = [x, y];
            } else {
                selected_position = null;
            }
            drawGrid();
        });

        canvas.addEventListener('mouseout', function() {
            selected_position = null;
            drawGrid();
        });
    }

    canvasElements.forEach((el, i) => {
        canvases[i] = document.getElementById(el);
        contexts[i] = canvases[i].getContext('2d');

        // Get CSS size
        const style_width = Number(getComputedStyle(canvases[i]).getPropertyValue("width").slice(0, -2));
        const style_height = style_width;  // make height equal to width to maintain square aspect ratio
        canvases[i].style.height = `${style_height}px`;

        // Set Canvas size based on CSS size and DPI
        canvases[i].setAttribute('width', style_width * dpi);
        canvases[i].setAttribute('height', style_height * dpi);

        // Different setup for different canvases
        if (i === 0) {
            setupFirstCanvas(canvases[i], contexts[i], dpi);
        } else if (i == 1) {
            setupSecondCanvas(canvases[i], contexts[i], dpi);
        } else if (i === 2) {
            setupThirdCanvas(canvases[i], contexts[i], dpi);
        }
    });
}
